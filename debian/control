Source: shutter
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: graphics
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Indep:
 gir1.2-ayatanaappindicator3-0.1 <!nocheck>,
 gir1.2-wnck-3.0 <!nocheck>,
 imagemagick <!nocheck>,
 libcarp-always-perl <!nocheck>,
 libfile-basedir-perl <!nocheck>,
 libfile-copy-recursive-perl <!nocheck>,
 libfile-which-perl <!nocheck>,
 libglib-object-introspection-perl <!nocheck>,
 libglib-perl <!nocheck>,
 libgoocanvas2-cairotypes-perl <!nocheck>,
 libgoocanvas2-perl <!nocheck>,
 libgtk3-imageview-perl (>= 10) <!nocheck>,
 libgtk3-perl <!nocheck>,
 libhttp-message-perl <!nocheck>,
 libimage-exiftool-perl <!nocheck>,
 libimage-magick-perl <!nocheck>,
 libjson-maybexs-perl <!nocheck>,
 libjson-perl <!nocheck>,
 liblocale-gettext-perl <!nocheck>,
 liblwp-protocol-https-perl <!nocheck>,
 libmoo-perl <!nocheck>,
 libnet-dbus-glib-perl <!nocheck>,
 libnet-dbus-perl <!nocheck>,
 libnet-oauth-perl <!nocheck>,
 libnumber-bytes-human-perl <!nocheck>,
 libpango-perl <!nocheck>,
 libpath-class-perl <!nocheck>,
 libproc-processtable-perl <!nocheck>,
 libproc-simple-perl <!nocheck>,
 libreadonly-perl <!nocheck>,
 librsvg2-common <!nocheck>,
 libsort-naturally-perl <!nocheck>,
 libtest-mockmodule-perl <!nocheck>,
 libtest-strict-perl <!nocheck>,
 libwww-mechanize-perl <!nocheck>,
 libwww-perl <!nocheck>,
 libx11-protocol-other-perl <!nocheck>,
 libx11-protocol-perl <!nocheck>,
 libxml-simple-perl <!nocheck>,
 procps <!nocheck>,
 xauth <!nocheck>,
 xdg-utils <!nocheck>,
 xvfb <!nocheck>,
Standards-Version: 4.7.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/shutter
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/shutter.git
Homepage: https://shutter-project.org/
Testsuite: autopkgtest-pkg-perl

Package: shutter
Architecture: all
Depends:
 gir1.2-ayatanaappindicator3-0.1,
 gir1.2-wnck-3.0,
 imagemagick,
 libcarp-always-perl,
 libfile-basedir-perl,
 libfile-copy-recursive-perl,
 libfile-which-perl,
 libglib-object-introspection-perl,
 libglib-perl,
 libgoocanvas2-cairotypes-perl,
 libgoocanvas2-perl,
 libgtk3-imageview-perl (>= 10),
 libgtk3-perl,
 libhttp-message-perl,
 libimage-exiftool-perl,
 libimage-magick-perl,
 libjson-maybexs-perl,
 libjson-perl,
 liblocale-gettext-perl,
 liblwp-protocol-https-perl,
 libmoo-perl,
 libnet-dbus-perl,
 libnet-dbus-glib-perl,
 libnet-oauth-perl,
 libnumber-bytes-human-perl,
 libpango-perl,
 libpath-class-perl,
 libproc-processtable-perl,
 libproc-simple-perl,
 libreadonly-perl,
 librsvg2-common,
 libsort-naturally-perl,
 libwww-mechanize-perl,
 libwww-perl,
 libx11-protocol-other-perl,
 libx11-protocol-perl,
 libxml-simple-perl,
 procps,
 xdg-utils,
 ${misc:Depends},
 ${perl:Depends},
Recommends:
 libavif-gdk-pixbuf,
Suggests:
 nautilus-sendto,
Description: feature-rich screenshot program
 Shutter is a feature-rich screenshot program. You can take a
 screenshot of a specific area, window, your whole screen, or even of
 a website - apply different effects to it, draw on it to highlight
 points, and then upload to an image hosting site, all within one
 window.
 .
 Features:
    * take a screenshot of your complete desktop, a rectangular area
    * take screenshot directly or with a specified delay time
    * save the screenshots to a specified directory and name them in a
      convenient way (using special wild-cards)
    * Shutter is fully integrated into the GNOME Desktop (TrayIcon etc.)
    * generate thumbnails directly when you are taking a screenshot
      and set a size level in %
    * Shutter session collection
          o keep track of all screenshots during session
          o copy screeners to clipboard
          o print screenshots
          o delete screenshots
          o rename your file
    * upload your files directly to Image-Hosters (e.g. imgur.com), retrieve
      all the needed links and share them with others
    * edit your screenshots directly using the embedded drawing tool
